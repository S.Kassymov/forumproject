﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ForumProject.Core.DTOs;
using ForumProject.Core.Entities;
using ForumProject.Core.Interfaces.Repositories;
using ForumProject.Infrastructure.Repositories;
using ForumProject.Infrastructure.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.IdentityModel.Tokens;
using AutoMapper;

namespace ForumProject.Api.Controllers
{
    [Authorize(Roles = Role.Moderator)]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public UserController(IUserRepository userRepository,IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }


        [HttpGet]
        public async Task<IActionResult> GetUserList()
        {
            var user = await _userRepository.GetUserList();
            var userToReturn = _mapper.Map<IEnumerable<UserDto>>(user);
            return user != null ? (IActionResult)Ok(userToReturn) : NoContent();
        }


        [HttpGet("{id:guid}")]
        public async Task<IActionResult> GetUserById(Guid id)
        {
            var user = await _userRepository.GetUserById(id);
            var userToReturn = _mapper.Map<IEnumerable<UserDto>>(user);
            return user != null ? (IActionResult)Ok(userToReturn) : NoContent();
        }


        [HttpGet("{input}")]
         public async Task<IActionResult> SearchUser(string input)
        {
            var user = await _userRepository.SearchUser(input);
            var userToReturn = _mapper.Map<IEnumerable<UserDto>>(user);
            return user != null ? (IActionResult)Ok(userToReturn) : NoContent();
        }

        
        [HttpPut("{id:guid}")]
        public async Task<ActionResult> ChangeUserRole(User u)
        {
            bool is_moderator = false;
            if (u.Role.Contains("Moderator"))
            {
                is_moderator = true;
            }
            
            User user = new User()
            {
                Name = u.Name,
                Surname = u.Surname,
                Username = u.Username,
                Email = u.Email,
                Role = is_moderator ? "RegularUser" : "Moderator"
            };

            if (await _userRepository.ChangeUserRole(u))
            {
                return Ok("A post was edited successufully!");
            }
            return BadRequest("Oops, something went wrong!");
        }

    }
}
