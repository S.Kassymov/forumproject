﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ForumProject.Core.DTOs;
using ForumProject.Core.Entities;
using ForumProject.Core.Interfaces.Repositories;
using Microsoft.AspNetCore.Authorization;
using AutoMapper;

namespace ForumProject.Api.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class PostsController : ControllerBase
    {
        private readonly IPostsRepository _postRepository;
        private readonly IMapper _mapper;

        public PostsController(IPostsRepository postRepository,IMapper mapper)
        {
            _postRepository = postRepository;
            _mapper = mapper;
        }



        [HttpGet]
        public async Task<IActionResult> GetPosts()
        {
            var post = await _postRepository.GetPosts();
            var postToReturn = _mapper.Map<IEnumerable<PostDto>>(post);
            return post != null ? (IActionResult)Ok(postToReturn) : NoContent();
        }



        [HttpGet("{id:guid}")]
        public async Task<IActionResult> GetPostById(Guid id)
        {
            var post = await _postRepository.GetPostById(id);
            var postToReturn = _mapper.Map<IEnumerable<PostDto>>(post);
            return post != null ? (IActionResult)Ok(postToReturn) : NoContent();
        }



        
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> AddPost(PostDto p)
        {
            Post pst = new Post()
            {
                Id = Guid.NewGuid(),
                Username = p.Username,
                Topic = p.Topic,
                Info = p.Info,
                Category = p.Category,
                Rating = 0,
                Date = DateTime.Now,
                AllAnswers = p.AllAnswers
            };
            if (await _postRepository.AddPost(pst))
            {
                return Ok("A new post was added successfully!");
            }

            return BadRequest("Oops, something went wrong!");
        }


        
        [Authorize]
        [HttpPut("{id}")]
        public async Task<ActionResult> EditPost(Guid id, PostDto p)
        {
            Post pst = new Post()
            {
                Id = id,
                Username = p.Username,
                Topic = p.Topic,
                Info = p.Info,
                Category = p.Category,
                Rating = p.Rating,
                Date = DateTime.Now,
                AllAnswers = p.AllAnswers
            };

            if (await _postRepository.EditPost(pst))
            {
                return Ok("A post was edited successufully!");
            }
            return BadRequest("Oops, something went wrong!");
        }

        [Authorize(Roles = Role.Moderator)]
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeletePost(Guid id)
        {
            if (await _postRepository.DeletePost(id))
            {
                return Ok("A post was deleted successfully!");
            }
            return BadRequest("Oops, something went wrong!");
        }

        [HttpGet("{input}")]
        public async Task<IActionResult> SearchPosts(string input)
        {
            var post = await _postRepository.SearchPosts(input);
            var postToReturn = _mapper.Map<IEnumerable<PostDto>>(post);
            return post != null ? (IActionResult)Ok(postToReturn) : NoContent();
        }

        [HttpGet("{category}")]
        public async Task<IActionResult> GetPostByCategory(string category)
        {
            var post = await _postRepository.GetPostByCategory(category);
            var postToReturn = _mapper.Map<IEnumerable<PostDto>>(post);
            return post != null ? (IActionResult)Ok(postToReturn) : NoContent();
        }

    }
}
