﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ForumProject.Core.DTOs;
using ForumProject.Core.Entities;
using ForumProject.Core.Interfaces.Repositories;
using ForumProject.Infrastructure.Repositories;
using ForumProject.Infrastructure.Data;
using Microsoft.AspNetCore.Authorization;
using AutoMapper;

namespace ForumProject.Api.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class AnswersController : ControllerBase
    {
        private readonly IAnswersRepository _answersRepository;
        private readonly IMapper _mapper;

        public AnswersController(IAnswersRepository answersRepository, IMapper mapper)
        {
            _answersRepository = answersRepository;
            _mapper = mapper;
        }


        [HttpGet]
        public async Task<IActionResult> GetAnswers()
        {
            var answers = await _answersRepository.GetAnswers();
            var answersToReturn = _mapper.Map<IEnumerable<AnswerDto>>(answers);
            return answers != null ? (IActionResult)Ok(answersToReturn) : NoContent();
        }


        [Authorize(Roles = Role.RegularUser)]
        [HttpPost("{id:guid}")]
        public async Task<ActionResult> AddAnswer(Guid id, AnswerDto a)
        {

            Answer ans = new Answer()
            {
                Id = Guid.NewGuid(),
                Username = a.Username,
                Info = a.Info,
                MasterId = a.MasterId,
                Rating = 0,
                Date = DateTime.Now
            };

            if (await _answersRepository.AddAnswer(ans))
            {
                return Ok("A new answer was added successfully!");
            }

            return BadRequest("Oops, something went wrong!");
        }

        
        [Authorize]
        [HttpPut("{id}")]
        public async Task<ActionResult> EditAnswer(Guid id, AnswerDto a)
        {
            Answer ans = new Answer()
            {
                Id = id,
                Username = a.Username,
                Info = a.Info,
                MasterId = a.MasterId,
                Rating = a.Rating,
                Date = DateTime.Now
            };

            if (await _answersRepository.EditAnswer(ans))
            {
                return Ok("A answer was edited successufully!");
            }
            return BadRequest("Oops, something went wrong!");
        }



        [Authorize(Roles = Role.Moderator)]
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteAnswer(Guid id)
        {
            if (await _answersRepository.DeleteAnswer(id))
            {
                return Ok("A post was deleted successfully!");
            }
            return BadRequest("Oops, something went wrong!");
        }

        [HttpGet("SortByDateAsc")]
        public async Task<IActionResult> SortAnswersByDateAsc()
        {
            
            var answers = await _answersRepository.SortAnswersByDateAsc();
            var answersSort = _mapper.Map<AnswerDto>(answers);
            return answers != null ? (IActionResult)Ok(answersSort) : NoContent();
        }

        [HttpGet("SortByDateDesc")]
        public async Task<IActionResult> SortAnswersByDateDesc()
        {
            var answers = await _answersRepository.SortAnswersByDateDesc();
            var answersSort = _mapper.Map<AnswerDto>(answers);
            return answers != null ? (IActionResult)Ok(answersSort) : NoContent();
        }

    }
}
