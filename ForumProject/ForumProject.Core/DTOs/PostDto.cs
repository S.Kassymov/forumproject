﻿using ForumProject.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ForumProject.Core.DTOs
{
    public class PostDto
    {
        public String Username { get; set; }
        public String Topic { get; set; }
        public String Info { get; set; }
        public DateTime Date { get; set; }
        public String Category { get; set; }
        public int Rating { get; set; }
        public ICollection<Answer> AllAnswers { get; set; }
    }
}
