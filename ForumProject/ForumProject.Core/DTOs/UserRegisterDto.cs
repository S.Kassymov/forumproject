﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ForumProject.Core.DTOs
{
    public class UserRegisterDto
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        [StringLength(15,MinimumLength =6, ErrorMessage ="Password length must be between 6 and 15")]
        public string Password { get; set; }
        public string Email { get; set; }
    }
}
