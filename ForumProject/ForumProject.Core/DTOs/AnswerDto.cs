﻿using ForumProject.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ForumProject.Core.DTOs
{
    public class AnswerDto
    {
        public String Username { get; set; }
        public String Info { get; set; }
        public Guid MasterId { get; set; }
        public int Rating { get; set; }
        public DateTime Date { get; set; }
    }
}
