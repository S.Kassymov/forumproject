﻿using AutoMapper;
using ForumProject.Core.DTOs;
using ForumProject.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ForumProject.Core.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            
            CreateMap<Post,PostDto> ();
            CreateMap<User, UserDto>();
            CreateMap<Answer, AnswerDto>();
            
        }
    }
}
