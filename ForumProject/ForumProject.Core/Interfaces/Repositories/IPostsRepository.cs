﻿using ForumProject.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ForumProject.Core.Interfaces.Repositories
{
    public interface IPostsRepository
    {
        Task<IEnumerable<Post>> GetPosts();
        Task<Post> GetPostById(Guid id);
        Task<bool> AddPost(Post p);
        Task<bool> EditPost(Post p);
        Task<bool> DeletePost(Guid id);
        Task<IEnumerable<Post>> SearchPosts(String s);
        Task<IEnumerable<Post>> GetPostByCategory(String category);
    }
}
