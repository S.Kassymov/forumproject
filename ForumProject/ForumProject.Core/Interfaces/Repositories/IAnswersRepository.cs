﻿using ForumProject.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ForumProject.Core.Interfaces.Repositories
{
    public interface IAnswersRepository
    {
        Task<IEnumerable<Answer>> GetAnswers();
        Task<bool> AddAnswer(Answer a);
        Task<bool> EditAnswer(Answer a);
        Task<bool> DeleteAnswer(Guid id);
        Task<IEnumerable<Answer>> SortAnswersByDateAsc();
        Task<IEnumerable<Answer>> SortAnswersByDateDesc();
    }
}
