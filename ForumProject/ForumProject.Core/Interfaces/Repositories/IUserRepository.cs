﻿using ForumProject.Core.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ForumProject.Core.Interfaces.Repositories
{
    public interface IUserRepository
    {
        Task<IEnumerable<User>> GetUserList();
        Task<IEnumerable<User>> GetUserById(Guid id);
        Task<IEnumerable<User>> SearchUser(string s);
        Task<bool> ChangeUserRole(User u);
    }
}