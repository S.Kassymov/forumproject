﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ForumProject.Core.Entities
{
    public class Answer
    {
        
        public Guid Id { get; set; }
        public String Username { get; set; }
        public String Info { get; set; }
        public Guid MasterId { get; set; }
        public Post MasterPost { get; set; }
        public DateTime Date { get; set; }
        public int Rating { get; set; }
    }
}
