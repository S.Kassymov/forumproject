﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ForumProject.Core.Entities
{
    public class Post
    {
        private object id;

        public Post()
        {
        }

        public Post(object id)
        {
            this.id = id;
        }

        public Guid Id { get; set; }
        public String Username { get; set; }
        public String Topic { get; set; }
        public String Info { get; set; }
        public DateTime Date { get; set; }
        public int Rating { get; set; }
        public String Category { get; set; }
        public ICollection<Answer> AllAnswers { get; set; } 
    }
}
