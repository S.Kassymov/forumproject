﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ForumProject.Core.Entities
{
    public class Role
    {
        public const string Moderator = "Moderator";
        public const string RegularUser = "RegularUser";
    }
}
