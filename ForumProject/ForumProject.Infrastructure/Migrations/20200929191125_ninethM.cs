﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ForumProject.Infrastructure.Migrations
{
    public partial class ninethM : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Answers_Posts_masterPostId",
                table: "Answers");

            migrationBuilder.RenameColumn(
                name: "masterPostId",
                table: "Answers",
                newName: "MasterPostId");

            migrationBuilder.RenameIndex(
                name: "IX_Answers_masterPostId",
                table: "Answers",
                newName: "IX_Answers_MasterPostId");

            migrationBuilder.AddColumn<Guid>(
                name: "MasterId",
                table: "Answers",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddForeignKey(
                name: "FK_Answers_Posts_MasterPostId",
                table: "Answers",
                column: "MasterPostId",
                principalTable: "Posts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Answers_Posts_MasterPostId",
                table: "Answers");

            migrationBuilder.DropColumn(
                name: "MasterId",
                table: "Answers");

            migrationBuilder.RenameColumn(
                name: "MasterPostId",
                table: "Answers",
                newName: "masterPostId");

            migrationBuilder.RenameIndex(
                name: "IX_Answers_MasterPostId",
                table: "Answers",
                newName: "IX_Answers_masterPostId");

            migrationBuilder.AddForeignKey(
                name: "FK_Answers_Posts_masterPostId",
                table: "Answers",
                column: "masterPostId",
                principalTable: "Posts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
