﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ForumProject.Infrastructure.Migrations
{
    public partial class tenthM : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Answers_Posts_MasterPostId",
                table: "Answers");

            migrationBuilder.DropIndex(
                name: "IX_Answers_MasterPostId",
                table: "Answers");

            migrationBuilder.DropColumn(
                name: "MasterPostId",
                table: "Answers");

            migrationBuilder.AddColumn<Guid>(
                name: "PostId",
                table: "Answers",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Answers_PostId",
                table: "Answers",
                column: "PostId");

            migrationBuilder.AddForeignKey(
                name: "FK_Answers_Posts_PostId",
                table: "Answers",
                column: "PostId",
                principalTable: "Posts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Answers_Posts_PostId",
                table: "Answers");

            migrationBuilder.DropIndex(
                name: "IX_Answers_PostId",
                table: "Answers");

            migrationBuilder.DropColumn(
                name: "PostId",
                table: "Answers");

            migrationBuilder.AddColumn<Guid>(
                name: "MasterPostId",
                table: "Answers",
                type: "uuid",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Answers_MasterPostId",
                table: "Answers",
                column: "MasterPostId");

            migrationBuilder.AddForeignKey(
                name: "FK_Answers_Posts_MasterPostId",
                table: "Answers",
                column: "MasterPostId",
                principalTable: "Posts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
