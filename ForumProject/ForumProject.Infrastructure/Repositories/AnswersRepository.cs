﻿using ForumProject.Infrastructure.Data;
using ForumProject.Core.Entities;
using ForumProject.Core.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ForumProject.Infrastructure.Repositories
{
    public class AnswersRepository : IAnswersRepository
    {
        private readonly DataContext _dbContext;

        public AnswersRepository(DataContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<bool> AddAnswer(Answer a)
        {
            _dbContext.Answers.Add(a);
            return (await _dbContext.SaveChangesAsync()) > 0;
        }

        public async Task<bool> DeleteAnswer(Guid id)
        {
            Answer ans = _dbContext.Answers.Find(id);
            _dbContext.Answers.Remove(ans);
            return (await _dbContext.SaveChangesAsync()) > 0;
        }

        public async Task<bool> EditAnswer(Answer a)
        {
            _dbContext.Answers.Update(a);
            return (await _dbContext.SaveChangesAsync()) > 0;
        }

        public async Task<IEnumerable<Answer>> GetAnswers()
        {
            return await _dbContext.Answers.ToListAsync();
        }

        public async Task<IEnumerable<Answer>> SortAnswersByDateAsc()
        {
            return await _dbContext.Answers.OrderBy(x => x.Date).ToListAsync();
        }

        public async Task<IEnumerable<Answer>> SortAnswersByDateDesc()
        {
            return await _dbContext.Answers.OrderByDescending(x=>x.Date).ToListAsync();
        }
    }
}
