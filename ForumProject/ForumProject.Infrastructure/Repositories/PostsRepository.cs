﻿using Microsoft.EntityFrameworkCore;
using ForumProject.Infrastructure.Data;
using ForumProject.Core.Entities;
using ForumProject.Core.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ForumProject.Infrastructure.Repositories
{
    public class PostsRepository : IPostsRepository
    {
        private readonly DataContext _dbContext;

        public PostsRepository(DataContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<bool> AddPost(Post p)
        {
            _dbContext.Posts.Add(p);
            return (await _dbContext.SaveChangesAsync()) > 0;
        }

        public async Task<bool> DeletePost(Guid id)
        {
            Post pst = _dbContext.Posts.Find(id);
            _dbContext.Posts.Remove(pst);
            return (await _dbContext.SaveChangesAsync()) > 0;
        }

        public async Task<bool> EditPost(Post p)
        {
            _dbContext.Posts.Update(p);
            return (await _dbContext.SaveChangesAsync()) > 0;
        }


        public async Task<Post> GetPostById(Guid id)
        {
            return await _dbContext.Posts.FindAsync(id);
        }

        public async Task<IEnumerable<Post>> GetPosts()
        {
            return await _dbContext.Posts.Include(a=>a.AllAnswers).ToListAsync();
        }

        public async Task<IEnumerable<Post>> SearchPosts(string input)
        {
            return await _dbContext.Posts.Where(p => p.Topic.Contains(input) || p.Info.Contains(input)).ToListAsync();
        }

        public async Task<IEnumerable<Post>> GetPostByCategory(string category)
        {
            return await _dbContext.Posts.Where(p => p.Category.Contains(category)).ToListAsync();
        }
    }
}
