﻿using ForumProject.Infrastructure.Data;
using ForumProject.Core.Entities;
using ForumProject.Core.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ForumProject.Infrastructure.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly DataContext _dbContext;
        public UserRepository(DataContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<bool> ChangeUserRole(User u)
        {
            _dbContext.Users.Update(u);
            return  (await _dbContext.SaveChangesAsync()) > 0 ;
        }

        public async Task<IEnumerable<User>> GetUserById(Guid id)
        {
            return await _dbContext.Users.Where(x => x.Id.Equals(id)).ToListAsync();
        }

        public async Task<IEnumerable<User>> GetUserList()
        {
            return await _dbContext.Users.ToListAsync();
        }

        public async Task<IEnumerable<User>> SearchUser(string input)
        {
            return await _dbContext.Users.Where(x=>x.Name.Contains(input) || x.Surname.Contains(input) || x.Username.Contains(input) || 
                                          x.Email.Contains(input)).ToListAsync();
        }
    }
}
